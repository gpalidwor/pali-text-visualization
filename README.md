# pali text visualization

_Author: Gareth Palidwor_

_Date: March 10th, 2019_

_Status: draft, subject to update_

## Abstract

Buddhist Pali discourses contain many _pericopes_, repeated phrases that are mneumonic device used to facilitate memorization. When heard or read, these pericopes have a compelling but elusive pattern which is difficult to imagine across a whole text.  Bioinformatics dot plots are used to visualize repeated patterns within and across genomic sequences and are readily adapted to textual analysis. Using a combination of dot plot and edit distance, a visualization of the Anapanasati Sutta (Ānāpānasatisuttaṃ) was created, which looked a bit like tartan. So I had some custom fabric woven based on that pattern.

## Introduction

Pericopes are "formulaic expressions or phrases that depict a recurrent situation or event and whose purpose is to facilitate memorization" (Analayo 2007). Pericopes are common in Buddhist Pali discourses. Upon hearing or reading, the repeated phrases become familiar and the pattern of use can be rhythmic and pleasing. 

The [dot plot](https://en.wikipedia.org/wiki/Dot_plot_\(bioinformatics\)), used in bioinformatics to visualize repeated patterns within and between genomic sequences, can easily be adapted to visualizing such patterns in text. 

In a dot plot, sequences are compared in a graphical form, with one sequence on the horizontal (X) axis and the other on the vertical axiy (Y). At the X,Y positions where the value of the both sequences at that position are the same, we place a dot. The sequences can be DNA, RNA or Proteins, and the two sequences may be different sequences or the same sequence compared with itself. The resulting plot provides an overall view or repeated patterns in genetic sequences. A plot of any given sequence against itself will yield a square plot with a diagonal line bisecting it. The pattern of the plot will be mirrored across the diagonal line. Duplications of the sequences appear as diagonals parallel to the bisecting diagonal, and reversed sequences appear as diganal lines of opposite slope. Depending on the sequence analyzed, these repeated patterns may be meaningful features such as protein domains or gene duplications.

**Figure 1: A dot plot of a mouse DNA against itself showing duplications from Wikipedia**

![A dot plot of a mouse DNA against itself showing duplications](rawdata/Mup_locus_showing_DNA_repeats.jpg)

Figure 1 is an example of a dot plot from a DNA contig (Fig 2, Maul 2002). In it we we see a block of repeated subsequences in the middle of the whole sequence, these appear as a block of diagonal lines parallel to the main diagonal.


### Method

Anapanasati sutta, Discourse on the Mindfulness of Breathing, was chosen as an example of an early discourse to analyze. The data was loaded from the Access to Insight website and the text of the anapanasati sutta was manually extracted from [DN_I](https://www.accesstoinsight.org/tipitaka/sltp/DN_I_utf8.html). 

An [R script](../src/anapanasati_sutta_dot_plot_test.R) was used to analyze the text. Non Pali elements such as formatting and position markers were removed and the text was tokenized using the R `tidytext` library. The restricted Damerau-Levenshtein distance as implemented in `stringdist` was calculated and the value placed in a matrix based on the compared string positions.

The first 600 words of the comparison were chosen for visualization in the form of a 600x600 matrix, rendered as a .png. The palette chosen has white representing an edit distance of zero (the same word on both axes), then ranging from grey to black, then red with increasing edit distance. The most important color is white, with the white points being the dot plot values. The red and black were chosen largely for the resulting tartan like pattern and aren't terribly significant.

### Results

**Figure 2: A dot plot visualization of the first 600 words of the anapanasati sutta**

![Sutta dot plot visualization](results/test_weft_600x.png)

In fhe plot of Figure 2 we see a number of repeated patterns. The white block in the lower left, for example, 

The online store [Weft](weft.design) can weave custom fabric directly from a provided image file. There are some restrictions in terms of the palette, but the visualization of Figure 2 was developed in an attempt to exploit the available palette. 

**Figure 3: The fabric woven by weft based on the visualization of Figure 2**

![Weft fabric rendering of sutta visualization](results/fabric_from_test_weft_600x.jpg)

### Discussion

So the pattern is lovely, but what does it mean?

The website [A Handful of Leaves](http://www.ahandfulofleaves.org/) has a [translation of the Ānāpānasatisuttaṃ](http://www.ahandfulofleaves.org/documents/anapanassatisutta_mn_118.pdf) has alternating paragraphs of Pali and matching English translation which we'll use below.

The first big block of white on the lower left extending from words 19 to 52 is the highlighted Pali text below. Note that the text in the dot plot shown includes the title as part of the text "ānāpānasati sutam"

_Evaṃ me sutaṃ: ekaṃ samayaṃ bhagavā sāvatthiyaṃ viharati pubbārāme
migāramātupāsāde sambahulehi abhiññātehi abhiññātehi therehi sāvakehi saddhiṃ,
**āyasmatā ca sāriputtena āyasmatā ca mahā moggallānena āyasmatā ca mahākassapena
āyasmatā ca mahā kaccāyanena āyasmatā ca mahākoṭṭhitena āyasmatā ca mahākappinena
āyasmatā ca mahācundena āyasmatā ca anuruddhena āyasmatā ca revatena āyasmatā ca
ānandena aññehi ca**  abhiññātehi abhiññātehi therehi sāvakehi saddhiṃ._

corresponding to this english text

_I have heard thus: At Sāvathi, in the Eastern Grove, at the mansion of Migāra’s mother;
there the Sublime One was abiding with many well known and distinguished elder
disciples: **Venerable Sāriputta, Venerable Mahā Moggallāna, Venerable Mahā Kassapa,
Venerable Mahā Kaccayana, Venerable Mahā Koṭṭhita, Venerable Mahā Kappina,
Venerable Mahā Cunde, Venerable Anuruddha, Venerable Revata, Venerable Ānanda**,
together with other well known and distinguished elder disciples._

The pali word "ca" appears to be a joining word like "and" joining all the named elders together, "āyasmatā" means venerable. So the first block pattern we're seeing is the repeating of a phrase something like "and venerable". 

Similarly, the next major pattern we see is due to the repeated use of the pali terms for "bhikkus", "elder", "advising" and "instructing";

_And there at that time, elder bhikkhus were advising and instructing new bhikkhus; some
elder bhikkhus were advising and instructing ten bhikkhus, some elder bhikkhus were
advising and instructing twenty bhikkhus, some elder bhikkhus were advising and
instructing thirty bhikkhus, some elder bhikkhus were advising and instructing forty
bhikkhus. Those new bhikkhus, advised and instructed by the elder bhikkhus, came to
know successive marks of distinction._

### Conclusion

This work is a  proof of concept of the application of dot plots to visualization of repeated patterns in texts. It seems particularly well suited to Pali discourses which are rich in pericopes. 

In future I plan to extend this to the comparison of any two arbitrary text blocks via an interactive web app using Shiny. There are a number of other bioinformatic techniques which may be also be interesting to explore with this Pali texts.

## Reference
Analayo, Bhikkhu. "Oral Dimensions of Pali Discourses: Pericopes, other Mnemonic Techniques and the Oral Performance Context." Canadian Journal of Buddhist Studies 3 (2007).

Brasington, Leigh. Right Concentration: A Practical Guide to the Jhanas. Shambhala Publications, 2015.

Maul, Jude E., et al. "The Chlamydomonas reinhardtii plastid chromosome: islands of genes in a sea of repeats." The Plant Cell 14.11 (2002): 2659-2679.

http://www.ahandfulofleaves.org/documents/anapanassatisutta_mn_118.pdf

